import time
import math


def lim(x):
    return (1+(1/x))**x


for i in range(1,1*10**16):
    if i%1000 ==0:
        print(lim(i))
    
print("For comparision, python thinks e is:")
print(math.e)
